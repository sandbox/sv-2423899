<?php

/**
 * @file
 *  Bootstrap Multiselect Widget admin/configuration functionality.
 */

/**
 * bootstrap_multiselect_widget.admin settings form.
 */
function bootstrap_multiselect_widget_settings_form() {
  // Selectors.
  $form['bootstrap_multiselect_widget_selectors'] = array(
    '#type' => 'textfield',
    '#title' => t('JQuery selector'),
    '#description' => t("You may optionally use those selectors to reduce the '&lt;select&gt;' DOM context which to use Bootstrap Multiselect Widget"),
    '#default_value' => variable_get('bootstrap_multiselect_widget_selectors', '')
  );

  // Pages.
  $form['bootstrap_multiselect_widget_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages List'),
    '#description' => t("Pages List"),
    '#default_value' => variable_get('bootstrap_multiselect_widget_pages')
  );
  
  return system_settings_form($form);
}

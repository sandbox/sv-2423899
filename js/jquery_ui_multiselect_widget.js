(function($) {
  Drupal.behaviors.multiSelect = {
    attach: function(context) {
      var selectors = Drupal.settings.bootstrap_multiselect_widget_selectors;

      $.each(selectors, function(index, element) {

        var selectText = 'Select All';

        if(element.indexOf('field_county') >= 0){
          selectText = 'All Counties';
        }

        $(element).multiselect({
          includeSelectAllOption: true,
          selectAllText: selectText,
          maxHeight: 400,
          buttonContainer: '<div class="dropdown" />'
        })
          .find('option[value="multiselect-all"]')
          .attr('disabled', 'disabled');

      });
      $(document).bind('state:required', function(e) {
        if (e.trigger) {
          if (e.value) {
            $(e.target).closest('.form-item, .form-wrapper').find('label.checkbox .form-required').remove();
          }
        }
      });

    }
  }
})(jQuery);
